(pushnew :docs *features*)
(ql:quickload '(:staple :staple-markdown
                :cl-fast-ecs :parachute :cl-mock-basic))
(staple:generate :cl-fast-ecs
                 :if-exists :supersede
                 :output-directory #P"public/")
