(in-package :cl-fast-ecs)

(deftype entity () "An entity type, basically just a `FIXNUM`." 'fixnum)

(defstruct (component-soa
            (:copier nil)
            (:predicate nil))
  (allocated 0 :type array-length)
  (exists nil :type simple-bit-vector)
  (min-entity array-dimension-limit :type entity)
  (max-entity -1 :type entity)
  (count 0 :type array-length)
  (slots (make-hash-table :test #'eq) :type hash-table))

(defstruct (storage (:constructor %make-storage))
  (entities-count 0 :type array-length)
  (entities-allocated 0 :type array-length)
  (deleted-entities nil :type (simple-array entity (*)))
  (deleted-entities-count 0 :type array-length)
  (component-storages nil :type (simple-array (or component-soa null) (*)))
  (component-created-bits nil :type simple-bit-vector)
  (component-removed-bits nil :type simple-bit-vector))

(declaim (ftype (function (&key (:initial-allocated array-length)) storage)
                make-storage))
(defun make-storage (&key (initial-allocated 32))
  "Creates and returns a new storage instance.

Optional `INITIAL-ALLOCATED` argument sets the initial count of pre-allocated
entities and defaults to 32.

See also `BIND-STORAGE`."
  (declare (special *component-registry* *component-registry-length*))
  (let* ((initial-allocated initial-allocated)
         (_ (assert (> (the array-length initial-allocated) 1)
                    (initial-allocated)
                    "Initial allocated size should be greater than 1."))
         (storage
           (%make-storage
            :entities-allocated initial-allocated
            :deleted-entities (make-array initial-allocated
                                          :element-type 'entity
                                          :initial-element -1)
            :component-storages
            (make-array
             *component-registry-length*
             :element-type 'component-soa
             :initial-contents
             (reverse
              (loop :for (nil component-ctor)
                    :on *component-registry* :by #'cddr
                    :collect (funcall (the function component-ctor)
                                      initial-allocated nil))))
            :component-created-bits (make-array *component-registry-length*
                                                :element-type 'bit
                                                :initial-element 1)
            :component-removed-bits (make-array *component-registry-length*
                                                :element-type 'bit
                                                :initial-element 0)))
         (storage-pointer (make-weak-pointer storage)))
    (declare (ignore _))
    (labels ((rebuild-component-storage (component-storage-index ctor)
               (declare (type array-index component-storage-index)
                        (type (function (array-length
                                         (or component-soa null))
                                        component-soa)
                              ctor))
               (if-let (storage (weak-pointer-value storage-pointer))
                 (symbol-macrolet
                     ((component-storages (storage-component-storages storage)))
                   (when (= component-storage-index (length component-storages))
                     (adjust-simple-arrayf
                      component-storages
                      (1+ component-storage-index)
                      :element-type '(or component-soa null)
                      :initial-element 'nil)
                     (adjust-simple-arrayf
                      (storage-component-created-bits storage)
                      (length component-storages)
                      :element-type 'bit :initial-element 1)
                     (adjust-simple-arrayf
                      (storage-component-removed-bits storage)
                      (length component-storages)
                      :element-type 'bit :initial-element 0))
                   (let ((old-component-storage
                           (svref component-storages component-storage-index)))
                     (setf (svref component-storages component-storage-index)
                           (funcall ctor (storage-entities-allocated storage)
                                    old-component-storage))))
                 (progn
                   (unhook *component-defined-hook*
                           #'rebuild-component-storage)
                   (unhook *component-redefined-hook*
                           #'rebuild-component-storage)))
               nil))
      (hook-up *component-defined-hook* #'rebuild-component-storage)
      (hook-up *component-redefined-hook* #'rebuild-component-storage)
      storage)))

(declaim (type storage *storage*))
(defvar *storage*)
(setf (documentation '*storage* 'variable)
      "Global storage instance to be used by virtually all library functions.

NOTE: it is not bound to any value initially.

See also `BIND-STORAGE`.")

(declaim (ftype (function (&key (:initial-allocated array-length)) storage)
                bind-storage))
(defun bind-storage (&key (initial-allocated 32))
  "Binds global variable `*STORAGE*` to the newly created storage instance.

See also `MAKE-STORAGE`."
  (setf *storage* (make-storage :initial-allocated initial-allocated)))
