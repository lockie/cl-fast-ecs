(in-package #:cl-user)

(defpackage cl-fast-ecs/tests
  (:use #:cl
        #:cl-fast-ecs
        #:parachute
        #:cl-mock)
  (:export #:run))
