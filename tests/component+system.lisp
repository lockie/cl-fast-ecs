(defpackage :test-component+system
  (:use :cl :cl-fast-ecs))
(in-package :test-component+system)

(defcomponent display
  (x nil :type t))

(defsystem draw
  (:components-rw (display)))
