(defpackage :test-systems-order
  (:use :cl :cl-fast-ecs))
(in-package :test-systems-order)

(defcomponent display
  (x nil :type t))

(defsystem draw
  (:components-ro (display)))

(defsystem calculate
  (:components-ro (display)
   :before (draw)))
