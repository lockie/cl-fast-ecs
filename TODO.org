* Tasks [68/163]
*** TODO post to Ultralisp
It got faster release cycle.

https://github.com/ultralisp/ultralisp/issues
*** TODO Flecs-like web UI?
*** TODO try implementing parent <-> child relationships
*** TODO try implementing relation triplets, object/relation/subject
just like in Flecs.
relaton = component? sorted index??
parent <-> child = also relation
*** TODO think about referencing entities by cartesian product in systems
*** TODO am I mutating constant lists anywhere? That's UB
*** TODO have a look at disassembly for same slot acessed twice
*** TODO let+ expanders for component data?..
*** TODO add typedefs in =WITH= macro accessor functions
I think I've seen hairy vector somewhere there
*** TODO implement entities generations
https://github.com/SanderMertens/flecs/blob/master/docs/Manual.md#generations
https://gamedev.stackexchange.com/a/146225/126329
*** TODO API to remove a bunch of entities (or all of them)
probably use some kind of predicate?
*** TODO salvage some ideas from here
https://github.com/JordanCpp/Lib-LDL
*** TODO minified printing method (using parameter ~*minified*~ or something)
*** TODO put coverage into CI
*** TODO readonly component slots, settable only on MAKE-COMPONENT?
*** TODO warnings for ineffective fns (e.g. slot-aref accessors) on first usage
*** TODO make :ECS-SAFE runtime feature by using (uiop:featurep) ???
*** TODO add storage parameter to hooks
*** TODO find use of sb-simd
*** TODO find use of utility's with-declaim-inline macro
https://github.com/gregcman/utility/blob/master/utility.lisp#L37
*** TODO component data monitoring for debug purposes
*** TODO integrate with for/iter libraries?
*** TODO add ~DEFINE-~ aliases for all ~DEF~ macros
*** TODO some kind of ~HAS-COMPONENT-STRUCTURE-CHANGED-P~ helper
might come in handy for serialization purposes
*** TODO batch entity deletion
*** TODO export NEW-CAPACITY?..
and/or add an ability to change reallocation size policy
*** TODO prevent possible double deletes
*** TODO make hooks a separate tiny library too
to extend the game engine with hooks. something like "tiny-hooks"
*** TODO ability to change/redefine existing hooks
*** TODO print known components with an unknown component error in ~MAKE-OBJECT~
*** TODO figure out quasiquote as a parameter to ~MAKE-OBJECT~ compiler macro
macroexpand first?.. or eval?...

only the component keyword should be constant, ctor parameters might not be

read https://www.lispworks.com/documentation/HyperSpec/Body/02_df.htm

https://clast.sourceforge.net/ ???

~*MACROEXPAND-HOOK*~?..
*** TODO dispatch make-component by keywords?..
so no fiddling with indices
*** TODO in ~RUN-SYSTEMS~, safely stack allocate
by limiting comonent-registry-length?
*** TODO ~DEFSYSTEM~: optimize by re-using existing rebuilders
with the same components?
*** TODO ~DEFSYSTEM~: component aliases?..
*** TODO ~DEFSYSTEM~: system execution order!
two more arrays, all systems + graph relations?..
*** TODO ungregister component functionality
*** TODO DEFCOMPONENT: if slot type is ECS:ENTITY, test if it really exists
also think on invalidation
*** TODO ~DEFCOMPONENT~: check whether the entity actually has the component
- in getters and ~SETF~ setters
- in ~DELETE-...~
*** TODO ~DEFCOMPONENT~: some sort of boundp trick for soa variable
see https://stackoverflow.com/q/37643841/1336774
also cl-environments and trivial-cltl2
or perhaps something about anaphoric macro?..
*** TODO ~DEFCOMPONENT~: rename accessors in ~%WITH-...~ macro to something better
so that error message is better than "undefined function (setf %x1234)"
*** TODO ~DEFCOMPONENT~: do the ~WITH-...~ macro for composite index too
mention in its docs it will (not) evaluate the value multiple times
*** TODO TEST-DELETE-ENTITY: add function to test entity existence
*** TODO add ~WITH-COMPONENTS~ macro to combine several calls to ~WITH*~ macros to one
*** TODO add MOP-based CLOS interface
see https://edoput.it/2023/11/19/data-oriented-clos.html
*** TODO PRINT-ENTITY: add some kind of minified output
something like ~*minimal*~ var akin to PRINT modifiers like ~*PRINT-MISER-WIDTH*~
*** TODO BINARY-SEARCH: consider making the search branchless
see https://mhdm.dev/posts/sb_lower_bound
see also https://yarchive.net/comp/linux/cmov.html
*** TODO DEFCOMPONENT: think on making index accessors inline
so that dynamic-extent would work on their results?
*** TODO DEFCOMPONENT: add slot parameter to calculate ~:INDEX~ load factor
a form with current entity count available as a variable
or a lambda with 1 arg or something
or something like (truncate (* x 0.5))
*** TODO DEFCOMPONENT: check whether indices should be actually updated
e.g. if value was actually changed
*** TODO DEFCOMPONENT: add API to query all entities having given component
*** TODO DEFCOMPONENT: nullable slot keyword arg
*** TODO DEFCOMPONENT: rehash hook for the index
*** TODO DEFCOMPONENT: single-slot component with no extra prefixes
or ability to set custom prefix
*** TODO DEFCOMPONENT: combined WITH- accessor macro for :UNIQUE NIL index case
*** TODO DEFCOMPONENT: handle acessors trying to access nonexistent component
*** TODO DEFCOMPONENT: handle access to deleted component in safe mode
*** TODO DEFCOMPONENT: think about prefabs mechanism, perhaps component copying
think about implementing via read-only components
*** TODO DEFCOMPONENT: set documentation on generated fns where applicable
*** TODO DEFCOMPONENT: handle already existing component in MAKE- ctor
*** TODO DEFCOMPONENT: automatically delete entities with no components?
*** TODO DEFCOMPONENT: change WITH-name macro BINDINGS to be like with-slots
Not using the order, but rather the names of the slots
*** TODO DEFCOMPONENT: type checks in MAKE-COMPONENT?
*** TODO DEFCOMPONENT: store component dtors in array, so no DELETE-COMPONENT generic
just regular function
*** TODO DEFCOMPONENT: the version of WITH- macro for indices that allows
deletion of entities while iterating, using ~COPY-LIST~ internally for bucket
*** TODO RUN-HOOK: restarts for failed hooked functions (like "retry", "unhook" etc)
*** TODO COMPONENT-SOA-EXISTS: invent better name
*** TODO DEFSYSTEM: some kind of ~RECOMPILE-SYSTEM~ restart for the case when
...the component structure has changed
*** TODO DEFSYSTEM: to avoid branching use set of entity indices instead of bitmap
*** TODO think about parallelizing ~BIT-AND~ in ~%REBUILD-ENTITIES-BITMAP~
*** TODO DEFSYSTEM: allow setting variable name for entity
*** TODO DEFSYSTEM: initforms for stateful "slots"?
*** TODO DEFSYSTEM: test adding/removing entities/components while iterating
*** TODO DEFSYSTEM: implement paging to save memory
https://skypjack.github.io/2021-06-12-ecs-baf-part-11/
Use powers of two as the page size argument?
Store set of indices instead of exists bit??
*** TODO DEFYSTEM: add ability to do system code profiling?..
*** TODO DEFSYSTEM: allow single system as after/before, DWIM-style
*** TODO make index fn with =MISSING-ERROR-P= return =NIL=?..
*** TODO RUN-SYSTEMS: add hook for every system?
*** TODO add function that ~LOAD~'s object definition
...in sense of ~MAKE-OBJECT~, from file
*** TODO MAKE-OBJECT: add restart to specify the component
*** TODO MAKE-OBJECT: add call to ~COERCE~ for slot values
to e.g. be able to supply fixnums instead of floats
Add some kind of generic helper method to be called explicitly by the user,
something in the spirit of ~FIXUP-COMPONENT-SLOTS~ or something that would
process the arguments plist (see alexandria:doplist).
*** TODO ci: Lispworks works, figure out the way to run it in CI (xvfb + xdotool?)
*** TODO tests: test storage hooks are self-deleted after GC
*** TODO tests: improve
See https://sabracrolleton.github.io/testing-framework
+ Fix CLISP: https://github.com/Shinmera/parachute/issues/49
+ Try https://github.com/tdrhq/slite
+ Or https://github.com/atlas-engineer/nyxt/issues/2199#issuecomment-1146808213
*** TODO readme: work on readme
https://github.com/matiassingers/awesome-readme
*** TODO readme: move Usage before Features?
*** TODO readme: add more badges
+ quickdocs! e.g. https://api.quickdocs.org/badge/cl-liballegro-nuklear.svg
+ test coverage https://img.shields.io/gitlab/pipeline-coverage/lockie/cl-liballegro-nuklear?style=plastic
+ codeclimate/etc? https://shields.io/category/analysis
+ stars https://img.shields.io/gitlab/stars/lockie/cl-liballegro-nuklear?color=yellow
+ discord chat https://shields.io/category/chat
*** TODO readme: try coveralls, it seems to support Lisp and is free for open-source
*** TODO documentation: explain ECS slang etc.
Also mention the implementation details (bigarray).
*** TODO documentation: add quickstart
*** TODO documentation: add implementation overview
*** TODO documentation: make a benchmark
https://orgmode.org/worg/org-tutorials/org-plot.html ,
https://orgmode.org/manual/Org-Plot.html ?
https://orgmode.org/worg/org-contrib/babel/languages/ob-doc-gnuplot.html ?
make result block a table, and feed that table to gnuplot
+ different lisp implementations
+ different ways to access large number of objects (ECS/regular AoS/CLOS)
+ compare with C (Flecs)!
*** TODO documentation: historical trivia
The Entity-Component-System (or ECS in short) architectural pattern, which is mostly used in video games, first emerged circa 2000 in [[https://gamedeveloper.com/design/postmortem-i-thief-the-dark-project-i-][Thief: The Dark Project]] and [[https://gamedevs.org/uploads/data-driven-game-object-system.pdf][Dungeon Siege]]. By the beginning of 2010s, it was [[https://t-machine.org/index.php/2007/09/03/entity-systems-are-the-future-of-mmog-development-part-1][popularized]] by Adam Martin while working on Operation Flashpoint: Dragon Rising. Later it made its way into the game development industry, including [[https://developer.apple.com/videos/play/wwdc2015/609][Apple GameplayKit]] framework and [[https://mcvuk.com/development-news/unity-unleashes-megacity][Unity3D engine]].
*** TODO read https://ajmmertens.medium.com/why-it-is-time-to-start-thinking-of-games-as-databases-e7971da33ac3
*** TODO read https://twitter.com/despair/status/1815889859141304651
*** DONE documentation: document that deleting from index
while iterating with WITH- is bad idea
*** DONE documentation: more docs for ~DEFCOMPONENT~!
+ document FINALIZE
+ document composite indices; mention that its name should not coincide with
  any slot name
+ mention that the result of the index fn should be declared dynamic
+ mention that ctor with existing component is not an error
+ document :start & :count for indices
*** DONE documentation: ~MAKE-ENTITY~: mention that entity order is always ascending
*** DONE add ~COPY-ENTITY~?..
*** DONE DEFSYSTEM: add :before/:after specifiers
Use topological sort for ~*system-registry*~.

See e.g. https://github.com/pkulev/topsort
*** DONE post to awesome-ecs
https://github.com/jslee02/awesome-entity-component-system
*** DONE implement reverse lookup using sorted component data
(was implemented using chained hash table)
*** DONE implement entity relationships via reverse lookups
+ entity <-> name
+ parent <-> child
+ object <-> action
have a look at https://austinmorlan.com/posts/entity_component_system/
*** DONE have =*storage*= as a global dynamic var instead of functions argument
have a look at =global-vars= library and use the same SBCL extension
~defvar~ perhaps, so free "the variable storage is unbound" message everywhere
+ setter like ~(set-storage)~ eq to ~(setf ecs:*storage* (make-storage))~
*** DONE rename =pre= and =post= to =initially= and =finally=
to be more like ~LOOP~.
and also return =finally= value as the system's return value
*** DONE implement dumper/pretty printer for entities
*** DONE move =doc/= folder to =docs/=? or =tests/= to =test/=
*** DONE optimize by using SVREF where applicable!
*** DONE optimize likes of %position-x-aref accessors
Those do extra memory hop. Remake as macro?
In ~with-~ and ~%with-~ macros, use ~with-gensym~ to make a bindings for SoA slots...
*** DONE docstring argument to all macroses
~ALEXANDRIA:PARSE-BODY~? or just manually,
see https://stackoverflow.com/a/66367812/1336774
*** DONE rearrange defintions in files according to logic
... and warnings in ~(ql:quickload :cl-fast-ecs :verbose t)~
*** DONE do sb-cover only when env var is set
*** DONE move doc template and css file to dedicated docs/ folder (+readme)
*** DONE fix Clasp compatibility (not SIMPLE-ARRAY in hooks)
*** DONE debug helper to print component data
*** DONE ~REPLACE-component~ function to copy data from one entity to another
very much like ~CL:REPLACE~
*** DONE DEFSOA: warn about field types using UPGRADED-ARRAY-ELEMENT-TYPE
*** DONE DEFCOMPONENT: think of renaming the accessors
~POSITION-X-AREF~ -> ~POSITION-X~
*** DONE DEFCOMPONENT: add some kind of create-or-update API
*** DONE DEFCOMPONENT: copy old data when redefining?
*** DONE DEFCOMPONENT: add hook in MAKE- ctor when adjusting SoA size (for GC)
*** DONE DEFCOMPONENT: use WITH-GENSYMS in WITH- macro
*** DONE DEFCOMPONENT: disable rw access in %WITH-SLOTS-RO macro in safe mode
Here's idea from Shinmera:
#+begin_src lisp
  (let (%x) (flet ((%x () %x) ((setf %x) (v) (error "lol")))
              (declare (inline %x)) (symbol-macrolet (x (%x)) ...))
#+end_src
*** DONE DEFCOMPONENT: test whether empty "tag" components work
*** DONE DEFCOMPONENT: <breaking> change WITH-name macro arg order
To be more like ~WITH-SLOTS~.
*** DONE DEFCOMPONENT: try replacing notinline decls with :compile-toplevel
*** DONE DEFCOMPONENT: custom finalizers?..
*** DONE DEFCOMPONENT: also call finalizer on DELETE-ENTITY!
*** DONE MAKE-STORAGE: provide initial allocated size?
*** DONE GROW-ARRAY: invent better name
*** DONE GROW-ARRAY: use smarter formula
https://github.com/python/cpython/blob/50e0866f/Objects/listobject.c#L70
https://github.com/facebook/folly/blob/main/folly/docs/FBVector.md
#+begin_src lisp
  (defun new-capacity (current-capacity)
    (the array-length (+ (ash current-capacity -1) current-capacity)))
#+end_src
*** DONE MAKE-ENTITY: call hook when growing storage
*** DONE DELETE-ENTITY: figure out what CCL dislikes
*** DONE DELETE-ENTITY: call hook when deleting entity?
*** DONE DEFHOOK: figure out what ECL dislikes
*** DONE RUN-HOOK: test removal while iterating
*** DONE HOOK-UP: use ADJUST-ARRAY*
*** DONE ADJUST-ARRAY*: use keyword args to be more like the regular one?
*** DONE DEFSYSTEM: add =:components-no= arg to skip entities with given component
*** DONE DEFSYSTEM: enumerate allowed components in error message
*** DONE DEFSYSTEM: pre-build a cache of entities to visit
Bit vector size of #entites. Put to closure.
Invalidate when components are added/removed.

Might also help with reverse relationships (parent -> children).

https://skypjack.github.io/2019-03-07-ecs-baf-part-2/
*** DONE DEFSYSTEM: optimize by storing min & max. entity for every component
Then in system body only loop until min of those maxs;
calculate in %REBUILD-ENTITIES-BITMAP and return in multiple values
*** DONE DEFSYSTEM: add ability to pass extra arguments to system function
Think =dt=.

https://stackoverflow.com/a/56742980/1336774
*** DONE DEFSYSTEM: ~:WITH~ argument to have variables initialized
...before the entity loop.
+ the ability to use multiple values, by specifying the list instead of symbol
*** DONE DEFSYSTEM: test that all given components are actually defined
*** DONE DEFSYSTEM: specify initialization code that runs before the loop once
*** DONE DEFSYSTEM: add ~:WHEN~ argument: the form evaluated every system invocation
...if it is NIL, the system is skipped this time
*** DONE DEFSYSTEM: add the ability to reference the component slots in `:WHEN`
*** DONE RUN-SYSTEMS: add a way to temporarily turn the system off
*** DONE RUN-SYSTEMS: having global dirty bits is a bad idea, rethink it
Imagine some system runs after another and adds some component. In the end of
~RUN-SYSTEMS~ all dirty bits are reset, so the first system won't know about that
new component.
*** DONE MAKE-OBJECT: test given components exist in safe mode
*** DONE MAKE-OBJECT: optimize by pre-calculating ~*component-registry*~ length.
*** DONE MAKE-OBJECT: implement as a compiler macro?
*** DONE ci: setup CI
*** DONE ci: use Franz Lisp
https://franz.com/ftp/pub/acl10.1express/linuxamd64.64/acl10.1express-linux-x64.tbz2
*** DONE ci: add Clasp
In fact there are pre-built Arch packages:
https://github.com/clasp-developers/clasp/wiki/Installing-Using-a-Package-Manager#installing-from-the-nightly-linux-package-repository
*** DONE tests: fix component redefinition test for CLISP
*** DONE tests: write access?..
#+begin_src lisp
#+nil (deftest test-component-write-access
  (testing "write component data"
    (let* ((storage (make-storage))
           (entity (make-entity storage))
           (x 42.0)
           (y 42.9))
      (make-position storage entity :x 0.0 :y 0.0)
      (setf (position-x-aref storage entity) x)
      (setf (position-y-aref storage entity) y)
      (ok (= x (position-x-aref storage entity)))
      (ok (= y (position-y-aref storage entity))))))
#+end_src
*** DONE readme: mention alpha quality and everything will change
*** DONE readme: pick logo from free icons
Like in here https://github.com/Lisp-Stat/lla
*** DONE readme: add link to documentation
*** DONE documentation: write one
*** DONE documentation: mention hooks' arguments
